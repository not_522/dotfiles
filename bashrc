export PS1='\w\$ '
setxkbmap -option ctrl:nocaps

#g++
export LIB_PATH="/home/not/competitive-programming/include/"
alias g++='g++ -O2 -Wall -Wextra -std=c++1z -I$LIB_PATH'

alias atcoder='python2 ~/OnlineJudgeHelper/oj.py --atcoder'
alias aoj='python2 ~/OnlineJudgeHelper/oj.py --aoj'
alias yukicoder='python2 ~/OnlineJudgeHelper/oj.py --yukicoder'
alias codeforces='python2 ~/OnlineJudgeHelper/oj.py --codeforces'

export EDITOR='emacs'

source $HOME/.py3/bin/activate

export LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH}
