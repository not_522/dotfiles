#!/usr/bash

# english directory name
LANG=C xdg-user-dirs-gtk-update

# chrome
sudo apt install libindicator7 libappindicator1
sudo dpkg -i google-chrome-stable_current_amd64.deb

# dropbox
sudo apt install libpango1.0-0 libpangox-1.0-0
sudo dpkg -i dropbox_2015.10.28_amd64.deb

# install commands
sudo apt install git emacs openjdk-8-jre-headless openjdk-8-jdk-headless gfortran

# clone repositories
git clone git@bitbucket.org:not_522/dotfiles.git
git clone https://github.com/not522/OnlineJudgeHelper.git

# set dotfiles
echo 'source dotfiles/bashrc' >> .bashrc
wget https://raw.githubusercontent.com/google/styleguide/gh-pages/google-c-style.el -O .emacs.d/google-c-style.el
ln -s dotfiles/emacs .emacs

# python enviroment
sudo apt install virtualenv
virtualenv -p python3 .py3
source .py3/bin/activate
pip install -U setuptools
pip install chainer scipy matplotlib
pip install git+https://github.com/icpc-jag/rime
deactivate

# mozc
# https://qiita.com/shishamo_dev/items/238f6e5060fb838827f6
sudo apt install ibus-mozc
killall ibus-daemon
ibus-daemon -d -x &

# OnlineJudgeHelper
git clone https://github.com/not522/OnlineJudgeHelper.git
